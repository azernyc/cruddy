<?php namespace Kalnoy\Cruddy\Entity\Fields\Types;

class Time extends DateTime {

    public $format = 'HH:mm';
}