<?php namespace Kalnoy\Cruddy\Entity\Fields\Types;

use Kalnoy\Cruddy\Entity\Fields\Input;

class Email extends Input {

    /**
     * The input type.
     *
     * @var string
     */
    protected $inputType = 'email';
}