<?php namespace Kalnoy\Cruddy\Entity\Fields\Types;

class Date extends DateTime {

    public $format = 'DD.MM.YYYY';
}