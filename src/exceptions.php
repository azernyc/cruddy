<?php namespace Kalnoy\Cruddy;

use RuntimeException;

class EntityNotFoundException extends RuntimeException {}