Cruddy
======

Laravel Eloquent CRUD without a line of code. This package allows to view, create, update
and delete eloquent models. All you need is to describe model's fields and columns.

More info @ [Laravel Forums](http://forums.laravel.io/viewtopic.php?id=15689)

See in action in [test application](https://github.com/lazychaser/cruddy-app).
